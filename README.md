# Telegram Plus

## Building
Install [clickable](clickable.bhdouglass.com/en/latest/) and the following packages:

    sudo apt-get install gperf libssl-dev zlib1g-dev

Then run once:

    ./build-tdlib-in-ubuntu-sdk-docker.sh

From now on you can build the app with:

    clickable
